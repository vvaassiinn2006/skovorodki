using Gtk;
using Gee;
 
class Skovorodki : Box{
     
    LevelBar bar1;
    LevelBar bar2;
    int level_oil = 10;
    int level_blina = 0;
    bool est_blin = false;
    int score = 0;
    bool is_game_runing = false;
 
    public Skovorodki() {
        orientation = Orientation.VERTICAL;
        homogeneous = true;
        
    }

    void blindebug () {
        prin ("level_oil = ", level_oil);
        prin ("level_blina = ", level_blina);
        prin ("est_blin = ", est_blin);
        prin ("score = ", score);
        prin ("is_game_runing = ", is_game_runing);
        prin ();
    }
 
    public void start(string[] args){
 
       
       
        
        var hbox = new Box (Orientation.HORIZONTAL, 0){
            homogeneous = true
        };
        var label_score = new Label (@"счет $score");
        bar1 = new LevelBar.for_interval (0.0, 10.0);
        bar2 = new LevelBar.for_interval (0.0, 10.0);
        Button btn_drop_oil = new Button.with_label ("Дабваить масло");
        Button btn_drop_blin = new Button.with_label ("Кинуть блин");
        Button btn_snyat_blin = new Button.with_label ("снять блин");
        Button startbtn = new Button.with_label ("Играть!");
        btn_snyat_blin.clicked.connect ((btn) => {
         if (level_blina <= 7) {
            error ("blin ne dojarilsa"); }
            if (est_blin == true) {
                est_blin = false;
                level_blina = 0;
                score++;
                prin("AAAAAAAAAAAAA", level_blina <= 7);
               
            } else {
                error ("no blins exist");
            }
            blindebug ();
        });
        btn_drop_blin.clicked.connect ((btn) => {
            if (est_blin == false) {
                est_blin = true;
                Timeout.add_seconds (1, () => {
                    if (is_game_runing == false) {
                        level_blina = 0;
                        bar2.value = level_blina;
                        return false;
                    }
                    if (est_blin == false) {
                        //  level_blina = 0;
                        bar2.value = 0;
                        return false;
                    }
                    level_blina++;
                    bar2.value = level_blina;
                    // bar2.show();
                    if (level_blina == 10)
                        error ("блин сгорел");
                    label_score.label = @"счет $score";
     
                    blindebug ();
     
                    return true;
                });
            } else {
                error ("там уже есть блин");
            }
        });
     
     
        startbtn.clicked.connect ((btn) => {
            prin (startbtn.label == "Играть!");
            prin (startbtn.label);
            stdout.flush ();
            if (startbtn.label == "Играть!") {
                is_game_runing = true;
                startbtn.label = "Закончить";
                Timeout.add_seconds (1, () => {
                    if (is_game_runing == false) {
                        //  level_oil = 0;
                        bar1.value = 0;
                        blindebug ();
                        return false;
                    }
                    level_oil--;
                    bar1.value = level_oil;
                    bar1.show ();
                    if (level_oil == 0)
                        error ("масло закончилось");
     
                    blindebug ();
                    return true;
                });
            } else {
                startbtn.label = "Играть!";
                is_game_runing = false;
                score = 0;
                level_oil = 10;
                est_blin = false;
                blindebug ();
            }
        });
     
        btn_drop_oil.clicked.connect ((btn) => {
     
            if (bar1.value + 2 > 10) {
                error ("слишком много масла");
            } else {
                level_oil += 2;
                bar1.value = level_oil;
            }
        });
        btn_drop_blin.clicked.connect ((btn) => {
        });
     
        this.add (label_score);
        this.add (new Label ("Уровень масла"));
        this.add (bar1);
        this.add (new Label ("Прогресс блина"));
        this.add (bar2);
        this.add (hbox);
        hbox.add (btn_drop_oil);
        hbox.add (btn_drop_blin);
        this.add (btn_snyat_blin);
        this.add (startbtn);
    
        
    }
 
}
 
void main(string[] args){

    Gtk.init (ref args);
    var window = new Window ();
    window.title = "Сковородки!";
    window.set_default_size (600, 300);
    window.destroy.connect (Gtk.main_quit);
    
    var skovorodki = new Skovorodki();
    skovorodki.start(args);
    window.add(skovorodki);
    window.show_all();
    
    Gtk.main ();
}
 
[Print] void prin (string s) {
    stdout.printf (s + "\n"); stdout.flush ();
}
